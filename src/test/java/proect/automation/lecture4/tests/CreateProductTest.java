package proect.automation.lecture4.tests;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import proect.automation.lecture4.BaseTest;

public class CreateProductTest extends BaseTest {

    @DataProvider (name = "LoginData")
    public Object[][] getData(){
       Object[][] data = new Object[1][2];
       data[0][0] = "webinar.test@gmail.com";
       data[0][1] = "Xcg7299bnSmMuRLp9ITw";
       return data;
       };

    @Test(dataProvider = "LoginData")
    public void createNewProduct(String login, String password) {
        actions.login(login, password);
        actions.createProduct();

    }
    @Test(dependsOnMethods = "createNewProduct")
    public void checkProduct(){
        actions.checkNewProduct();
    }


}
