package proect.automation.lecture4;


import proect.automation.lecture4.model.ProductData;
import proect.automation.lecture4.utils.Properties;
import proect.automation.lecture4.utils.logging.CustomReporter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private By catalogueLink = By.cssSelector("#subtab-AdminCatalog");
    private By productsLink = By.cssSelector("#subtab-AdminProducts");
    private By createNewProduct = By.id("page-header-desc-configuration-add");
    private By inputNameNewProduct = By.id("form_step1_name_1");
    private By inputQuantNewProduct = By.id("form_step1_qty_0_shortcut");
    private By inputPriceNewProduct = By.id("form_step1_price_shortcut");
    private By submitNewProduct = By.id("submit");
    private By switchOn = By.className("switch-input");
    private By productPrice = By.className("current-price");
    private By productQty = By.xpath("//div[3]/div/div[2]/div[1]/span");
    private By nextPage = By.xpath("//a[@rel='next']");//кн Вперед
    private By numberPage = By.xpath("//a[@class=\"js-search-link\"]");
    private By allProducts = By.cssSelector(".all-product-link.pull-xs-left.pull-md-right.h4");//кн ВсеТовары

    ProductData productData = ProductData.generate(); //генерируем Новый продукт
    String newProductName = productData.getName(); //получаем Имя продукта
    int qtyNewProduct = productData.getQty();   //получаем Количество продукта
    String qtyNewProductString = String.valueOf(qtyNewProduct);//Приводим кол-во в Стринг
    String priceNewProduct = productData.getPrice();   //получаем Цену продукта

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    public void login(String login, String password) {
        CustomReporter.log("Login user " + login);
        driver.navigate().to(Properties.getBaseAdminUrl());
        driver.findElement(By.id("email")).sendKeys(login);
        driver.findElement(By.id("passwd")).sendKeys(password);
        driver.findElement(By.name("submitLogin")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main")));
    }
    public void createProduct() {
        CustomReporter.log("Login user ");
        WebElement catalogueLink = driver.findElement(this.catalogueLink);    //кнопка Каталог
        WebElement productsLink = driver.findElement(this.productsLink);  //кнопка Товары
        //создаем цепь действий с неявным элементом (кнопка Товары)
        Actions actions = new Actions(driver);
        actions.moveToElement(catalogueLink).click(productsLink);
        Action mouseOverAndClick = actions.build();
        mouseOverAndClick.perform();
        waitForContentLoad(createNewProduct); //ожидание
        WebElement createNewProduct = driver.findElement(this.createNewProduct); //кнопка Создать Новый товар
        createNewProduct.click(); //Создаем Новый ТОвар
        waitForContentLoad(inputNameNewProduct);
        WebElement inputNameNewProduct = driver.findElement(this.inputNameNewProduct);//поле Ввода Нового имени
        //создаем цепь действий со СЛОЖНЫМ элементом (Поле Имени Нового Товара)
        Actions actions1 = new Actions(driver);
        actions1.moveToElement(inputNameNewProduct).click(inputNameNewProduct);
        Action mouseOver1 = actions1.build();
        mouseOver1.perform(); //Устанавливаем курсор в поле Ввода Имени Нового ТОвара
        inputNameNewProduct.sendKeys(newProductName); //Вводим Имя в Поле
        WebElement inputQuantNewProduct = driver.findElement(this.inputQuantNewProduct);//поле Кол-во
        inputQuantNewProduct.click();                                                   //уснКурсор в поле
        inputQuantNewProduct.sendKeys(Keys.BACK_SPACE);                                 //чистим поле
        inputQuantNewProduct.sendKeys(qtyNewProductString);                             //вводим Кол-во
        WebElement inputPriceNewProduct = driver.findElement(this.inputPriceNewProduct); //Цена
        inputPriceNewProduct.click();                               //курсор в поле
        for(int i=0; i<6; i++) {                                    //чистим поле
            inputPriceNewProduct.sendKeys(Keys.BACK_SPACE);
        }
        inputPriceNewProduct.sendKeys(priceNewProduct);         //вводим Новую Цену
        WebElement submitNewProduct = driver.findElement(this.submitNewProduct); //кн Сохранить
        submitNewProduct.click();
        WebElement switchOn = driver.findElement(this.switchOn);  //Активировать
        switchOn.click();
    }
            //Проверка Нового продукта
    public void checkNewProduct(){
        driver.navigate().to(Properties.getBaseUrl());
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main")));
        WebElement allProducts = driver.findElement(this.allProducts); //кн "Все товары"
        allProducts.click(); //
        waitForContentLoad(By.id("main"));
            //Если Наш товар Не на первой странице, то
        WebElement nextPage = driver.findElement(this.nextPage); //кн. Вперед
        List<WebElement> pages = driver.findElements(numberPage); //2 и следующие страницы
        if(pages.size() > 0 ){
            nextPage.click();   //переход на вторую страницу
        }
        WebElement newProductLink = driver.findElement(By.linkText(newProductName)); //наш продукт
        Assert.assertEquals("**There is not such ProductName**", newProductLink.getText(), newProductName);
        newProductLink.click(); //Открыть Товар
        waitForContentLoad(productPrice);
        WebElement productPrice = driver.findElement(this.productPrice);
        WebElement productQty = driver.findElement(this.productQty);
        Assert.assertEquals("**Not such Price**", priceNewProduct +" ₴", productPrice.getText());
        Assert.assertEquals("**Not such Qty**", qtyNewProductString + " Товары", productQty.getText());

    }

    public void waitForContentLoad(By element) {
         wait.until(ExpectedConditions.presenceOfElementLocated(element));
      }
}
